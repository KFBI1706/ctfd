#!/usr/bin/env python
import glob
import os
import sys
import yaml
import hashlib
import shutil
from CTFd import create_app
from CTFd.models import Users, Teams, Challenges, Flags, Awards, ChallengeFiles, Fails, Solves, Hints, Tags
from CTFd.plugins.dynamic_challenges import DynamicValueChallenge
from CTFd.plugins.dynamic_challenges import DynamicChallenge, DynamicValueChallenge
#from CTFd.plugins.flags import CTFdStaticFlag

from sqlalchemy.orm.exc import NoResultFound

DECAY_LIMIT = 24


def directories(path):
    return [path + "/" + d for d in os.listdir(path) if not os.path.isfile(path+"/"+d)]

# create sanity check task to use as decay limit for dynamic challenge

UPLOADS_DIR = "/var/uploads/"

app = create_app()

challs = "/challs/challenges"

failed = []
with app.app_context():
    db = app.db

    for category in directories(challs):
        for chalPath in directories(category):
            category, task = chalPath.split("/")[-2:]
            try:
                with open(chalPath + "/chal.yml", "r") as f:
                    vals = yaml.safe_load(f)
                with open(chalPath + "/flag.txt", "r") as f:
                    flag = f.read().strip()

                try:
                    chal = Challenges.query.filter_by(
                        name=task, category=category).one()
                    chal.description = vals['description']
                    chal.value = vals['points']
                    chal.category = category
                except NoResultFound:
                    challenge_data = {
                        "name": task,
                        "category": category,
                        "description": vals["description"],
                        "value": vals['points'],
                        "decay": DECAY_LIMIT,
                        "minimum": 1,
                        "state": "visible",
                        "type": "dynamic",
                    }
                    chal = DynamicChallenge(**challenge_data)

                    #chal = Challenges(
                    #    name=task,
                    #    description=vals['description'],
                    #    value=vals['points'],
                    #    category=category
                    #)
                except Exception as e:
                    print("ERROR" + e)
                db.session.add(chal)
                db.session.commit()

                try:
                    f = Flags.query.filter_by(challenge_id=chal.id).one()
                    f.content = flag
                    f.data = "case_sensitive"
                except NoResultFound:
                    f = Flags(
                        challenge_id=chal.id,
                        content=flag,
                        data="case_sensitive",
                        type="static"
                    )
                except Exception as e:
                    print(e)
                db.session.add(f)
                db.session.commit()

                chal_hint_query = Hints.query.filter_by(challenge_id=chal.id)
                hints = chal_hint_query.all()
                if hints is not None and vals['hints'] is not None and len(hints) == len(vals['hints']):
                    if sorted([hint.content for hint in hints]) != sorted(vals['hints']):
                        for i in range(len(hints)):
                            hints[i].content = vals['hints'][i]
                            db.session.add(hints[i])
                elif vals['hints'] is not None:
                    chal_hint_query.delete()
                    for hint in vals['hints']:
                        h = Hints(
                            challenge_id=chal.id,
                            content=hint
                        )
                        db.session.add(h)
                db.session.commit()

                chal_tag_query = Tags.query.filter_by(challenge_id=chal.id)
                tags = chal_tag_query.all()
                if tags is not None and vals['tags'] is not None and len(tags) == len(vals['tags']):
                    if sorted([tag.value for tag in tags]) != sorted(vals['tags']):
                        for i in range(len(tags)):
                            tags[i].value = vals['tags'][i]
                            db.session.add(tags[i])
                elif vals['tags'] is not None:
                    chal_tag_query.delete()
                    for tag in vals['tags']:
                        t = Tags(
                            challenge_id=chal.id,
                            value=tag
                        )
                        db.session.add(t)
                db.session.commit()

                chal_file_query = ChallengeFiles.query.filter_by(
                    challenge_id=chal.id)
                files = [file.location.split("/")[-1]
                         for file in chal_file_query.all()]
                uploads = os.listdir(chalPath + "/uploads")

                chal_file_query.delete()

                for filename in [ upload for upload in uploads if upload != ".keep"]:
                    md5hash = hashlib.md5(filename.encode('utf-8')).hexdigest()
                    chal_file = ChallengeFiles(
                        challenge_id=chal.id,
                        location=md5hash + '/' + filename
                    )
                    db.session.add(chal_file)

                    folder = UPLOADS_DIR + md5hash
                    if not os.path.exists(folder):
                        os.mkdir(folder)
                    shutil.copy(chalPath + "/uploads/" +
                                filename, folder + '/' + filename)
                db.session.commit()

                # TODO Add tags?
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                failed.append((category,task, (e, exc_type, fname, exc_tb.tb_lineno)))


    db.session.commit()
    db.session.close()
    print(f"FAILED {failed}")
